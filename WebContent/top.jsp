<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>掲示板課題</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="settings">設定</a>
				<a href="logout">ログアウト</a>
				<a href="newMessage">新規投稿</a>
				<a href="admin">ユーザー管理</a>
			</c:if>
		</div>
	</div>

	<c:if test="${ not empty loginUser }">
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="login_id">
				@
				<c:out value="${loginUser.login_id}" />
			</div>
		</div>
	</c:if>



	<c:if test="${ not empty loginUser }">
		<div class="posts">
			<c:forEach items="${posts}" var="post">
				<div class="posts">

					<form action="deleteMessage" method="post">
					<input type="submit" value="削除">
					<input type="hidden" name="post_id" value="${post.id}" />
					</form>
					<br />

					<div class="user_id">
						【ユーザーID】<br />
						<c:out value="${post.user_id}" />

					</div>
					<br />

					<div class="subject">
						<span class="subject">
						【件名】<br /> <c:out
								value="${post.subject}" /></span>
					</div>
					<br />

					<div class="this_paper">
						【本文】<br />
						<c:out value="${post.this_paper}" />
					</div>
					<br />

					<div class="category">
						【カテゴリー】<br />
						<c:out value="${post.category}" />
					</div>
					<br />

					<div class="date">
						【投稿時間】<br />
						<fmt:formatDate value="${post.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<br />


						<div class="comments">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${post.id == comment.post_id}" >
									<div class="comment">
										<div class="this_paper-name">
											<span class="name"><c:out value="${comment.name}" /></span>
											<span class="this_paper"><c:out value="${comment.this_paper}" /></span>
										</div>
										<div class="date">
											<fmt:formatDate value="${comment.created_date}"
														pattern="yyyy/MM/dd HH:mm:ss" />
										</div>
									</div>
								<form action="deleteComment" method="post">
								<input type="submit" value="削除">
								<input type="hidden" name="comment_id" value="${comment.id}" />
								</form>
								</c:if>
							</c:forEach>
						</div>
					<c:if test="${ not empty loginUser }">
						<div class="comments">
							<div class="form-area">
								<form action="comment" method="post">
									<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
									<br /> <br /> <input type="submit" value="コメント">
									<input type="hidden" name="post_id" value="${post.id}" /> <br />
									<br />
									<br />
								</form>
							</div>
						</div>
					</c:if>
				</div>
			</c:forEach>
		</div>
	</c:if>




	<div class="copyright">Copyright(c)NozawaKazuki</div>
</body>
</html>