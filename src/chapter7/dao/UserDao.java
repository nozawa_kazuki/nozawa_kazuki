package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.beans.UserAdmin;
import chapter7.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", created_date");
            sql.append(", is_stopped");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // is_stopped
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            ps.setInt(6, user.getIs_stopped());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String login_id,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (login_id = ?) AND password = ? AND is_stopped = 0";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                Timestamp created_date = rs.getTimestamp("created_date");
                int is_stopped = rs.getInt("is_stopped");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setPassword(password);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setCreated_date(created_date);
                user.setIs_stopped(is_stopped);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public List<UserAdmin> getUserAdmin(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("branches.name as branch_name, ");
            sql.append("positions.name as position_name, ");
            sql.append("users.created_date as created_date, ");
            sql.append("users.is_stopped as is_stopped ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

//            System.out.println(ps);

            ResultSet rs = ps.executeQuery();
            List<UserAdmin> ret = toUserAdminList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserAdmin> toUserAdminList(ResultSet rs)
            throws SQLException {

        List<UserAdmin> ret = new ArrayList<UserAdmin>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branch_name = rs.getString("branch_name");
                String position_name = rs.getString("position_name");
                Timestamp created_date = rs.getTimestamp("created_date");
                int is_stopped = rs.getInt("is_stopped");


//                System.out.println(ret);

                UserAdmin admin = new UserAdmin();
                admin.setId(id);
                admin.setLogin_id(login_id);
                admin.setPassword(password);
                admin.setName(name);
                admin.setBranch_name(branch_name);
                admin.setPosition_name(position_name);
                admin.setCreated_date(created_date);
                admin.setIs_stopped(is_stopped);

//                System.out.println(comment.getId());

                ret.add(admin);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void state(Connection connection, User state) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users ");
            sql.append("SET is_stopped = ? ");
            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, state.getIs_stopped());
            ps.setInt(2, state.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }



}