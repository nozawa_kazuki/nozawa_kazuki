package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter7.beans.Comment;
import chapter7.beans.Message;
import chapter7.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("subject");
            sql.append(", this_paper");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // subject
            sql.append(", ?"); // this_paper
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getSubject());
            ps.setString(2, message.getThis_paper());
            ps.setString(3, message.getCategory());
            ps.setInt(4, message.getUser_id());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
        public void delete(Connection connection, Message message) {

            PreparedStatement ps = null;
            try {
                StringBuilder sql = new StringBuilder();
                sql.append("DELETE ");
                sql.append("FROM posts ");
                sql.append("where id=");
                sql.append("?");

                ps = connection.prepareStatement(sql.toString());

                ps.setInt(1, message.getId());

                ps.executeUpdate();

            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            } finally {
                close(ps);
            }

    }

        public void delete(Connection connection, Comment comment) {

            PreparedStatement ps = null;
            try {
                StringBuilder sql = new StringBuilder();
                sql.append("DELETE ");
                sql.append("FROM comments ");
                sql.append("where id=");
                sql.append("?");

                ps = connection.prepareStatement(sql.toString());

                ps.setInt(1, comment.getId());

                ps.executeUpdate();

            } catch (SQLException e) {
                throw new SQLRuntimeException(e);
            } finally {
                close(ps);
            }

    }


}