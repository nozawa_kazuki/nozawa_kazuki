package chapter7.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String subject;
    private String this_paper;
    private String category;
    private Date createdDate;
    private int user_id;

    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getSubject() {
    	return subject;
    }
    public void setSubject(String subject) {
    	this.subject = subject;
    }

    public String getThis_paper() {
    	return this_paper;
    }
    public void setThis_paper(String this_paper) {
    	this.this_paper = this_paper;
    }

    public String getCategory() {
    	return category;
    }
    public void setCategory(String category) {
    	this.category = category;
    }

    public Date getCreatedDate() {
    	return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    public int getUser_id() {
    	return user_id;
    }
    public void setUser_id(int user_id) {
    	this.user_id = user_id;
    }
 }