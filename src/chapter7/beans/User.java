package chapter7.beans;

import java.io.Serializable;
import java.util.Date;

//import chapter6.controller.SignUpServlet;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String password;
    private String name;
    private int branch_id;
    private int position_id;
    private Date created_date;
    private int is_stopped;


    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getLogin_id() {
    	return login_id;
    }
    public void setLogin_id(String login_id) {
    	this.login_id = login_id;
    }

    public String getPassword() {
    	return password;
    }
    public void setPassword(String password) {
    	this.password = password;
    }

    public String getName() {
    	return name;
    }
    public void setName(String name) {
    	this.name = name;
    }

    public int getBranch_id() {
    	return branch_id;
    }
    public void setBranch_id(int branch_id) {
    	this.branch_id = branch_id;
    }

    public int getPosition_id() {
    	return position_id;
    }
    public void setPosition_id(int position_id) {
    	this.position_id = position_id;
    }

    public Date getCreated_date() {
    	return created_date;
    }
    public void setCreated_date(Date created_date) {
    	this.created_date = created_date;
    }

    public int getIs_stopped() {
    	return is_stopped;
    }
    public void setIs_stopped(int is_stopped) {
    	this.is_stopped = is_stopped;
    }
}