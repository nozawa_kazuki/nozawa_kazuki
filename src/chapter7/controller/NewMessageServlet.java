package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Message;
import chapter7.beans.User;
import chapter7.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("newMessage.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> posts = new ArrayList<String>();

        if (isValid(request, posts) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message post = new Message();
            post.setSubject(request.getParameter("subject"));
            post.setThis_paper(request.getParameter("this_paper"));
            post.setCategory(request.getParameter("category"));
            post.setUser_id(user.getId());

//            System.out.println(post);

            new MessageService().register(post);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorposts", posts);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> posts) {

        String this_paper = request.getParameter("this_paper");

        if (StringUtils.isEmpty(this_paper) == true) {
            posts.add("メッセージを入力してください");
        }
        if (140 < this_paper.length()) {
            posts.add("140文字以下で入力してください");
        }
        if (posts.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}